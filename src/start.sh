#!/bin/bash

# Custom password
if ! test -z "$UDX11_PASSWD"; then
    x11vnc -storepasswd "$UDX11_PASSWD" ./.vnc_passwd
fi

# Default Display
if test -z "$DISPLAY"; then
    export DISPLAY=:0
fi

# Default Resolution
if test -z "$RESOLUTION"; then
    export RESOLUTION=1920x1080x24
fi

# (1) Server X
echo "Start Xvfb"
! pgrep -a Xvfb && Xvfb $DISPLAY -screen 0 $RESOLUTION &
sleep 1

# (2) Windows manager
if which fluxbox &>/dev/null; then
    echo "Start fluxbox"
    ! pgrep -a fluxbox && fluxbox 2>/dev/null &
fi
sleep 1

# (3) x11 Server
echo "Start x11vnc"
! pgrep -a x11vnc && x11vnc -bg -forever -usepw -quiet -rfbauth ./.vnc_passwd -display WAIT$DISPLAY &

# (4) NoVNC on the Web
echo "Start noVNC"
./noVNC/utils/launch.sh --ssl-only --cert /etc/ssl/novnc.pem --listen 6080 --vnc 127.0.0.1:5900

# Debug
#xdpyinfo -display $DISPLAY

# Loop
touch ./loop
tail -f ./loop
