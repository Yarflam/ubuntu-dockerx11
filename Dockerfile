FROM ubuntu
MAINTAINER Yarflam

# Install standards dependencies
RUN apt-get update && apt-get install -y curl\
    wget nano apt-transport-https lsb-release\
    ca-certificates git openssl sudo

# Install english pack
RUN apt-get update && apt-get install -y --no-install-recommends locales &&\
    locale-gen en_US.UTF-8 &&\
    echo "environment=LANG=\"en_US.utf8\", LC_ALL=\"en_US.UTF-8\", LC_LANG=\"en_US.UTF-8\"" > /etc/default/locale
RUN chmod 0755 /etc/default/locale
ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8
ENV DEBIAN_FRONTEND=noninteractive

# Install XVFB, x11vnc, numpy
RUN apt-get update && apt-get install -y xvfb x11vnc python3-numpy

# Create a user
RUN useradd -ms /bin/bash udx11
RUN usermod -aG sudo udx11
USER udx11
WORKDIR /home/udx11

# Configure x11vnc
RUN x11vnc -storepasswd "dockerpass" ./.vnc_passwd
RUN chmod u+rwx,g+rx-w,o-rwx ./.vnc_passwd

# Configure users
USER root
RUN echo "root:dockerpass" | chpasswd
RUN echo "udx11:dockerpass" | chpasswd

# Configure NoVNC
WORKDIR /etc/ssl
RUN openssl req \
    -new \
    -newkey rsa:4096 \
    -days 365 \
    -nodes \
    -x509 \
    -subj "/C=FR/ST=Paris/L=Paris/O=noVNC/CN=www.novnc.local" \
    -keyout novnc.pem \
    -out novnc.pem
RUN chmod 644 novnc.pem

# Install fluxbox
RUN apt-get update && apt-get install -y fluxbox

# Install Software
RUN apt-get update &&\
    apt-get install -y terminator firefox

# Configure the project
USER udx11
WORKDIR /home/udx11
RUN git clone https://github.com/novnc/noVNC.git
COPY ./src/start.sh .
RUN sed -i 's/\r//' ./start.sh
RUN chmod +x ./start.sh
RUN sed -i 's/$(hostname)/0.0.0.0/g' ./noVNC/utils/launch.sh

# Background
COPY ./src/tux.jpg .
RUN mkdir /home/udx11/.fluxbox && \
    echo '$tile $full|/home/udx11/tux.jpg||:0.0' > /home/udx11/.fluxbox/lastwallpaper

EXPOSE 6080

CMD [ "/home/udx11/start.sh" ]
