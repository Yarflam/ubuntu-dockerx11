# Ubuntu DockerX11

![version](https://img.shields.io/badge/OS-Ubuntu--Focal-orange.svg)
![version](https://img.shields.io/badge/Proc-x86--x64-darkgreen.svg)
![version](https://img.shields.io/badge/Release-v0.1-blue.svg)

## Abstract

Ubuntu DockerX11 is a container with:
- **Xvfb** - server X
- **Fluxbox** - windows manager
- **x11vnc** - server to share the desktop
- **noVNC** - web interface to control x11vnc

Installed software:
- **Firefox** - browser
- **Terminator** - advanced terminal

## Prerequisites

You must have installed a Docker instance on your machine.

```bash
sudo apt-get update &&\
sudo apt-get install -y snap docker-compose &&\
snap install docker
```

## Start a container

### 1. Docker Hub Register

The image [yarflam/udx11](https://hub.docker.com/r/yarflam/udx11) is available on Docker Hub.

```bash
sudo docker login &&\
sudo docker pull yarflam/udx11
```

Example to start a container:

```bash
sudo docker run \
    --name udx11 \
    --rm \
    --detach \
    --env RESOLUTION=1920x1080x24 \
    --env DISPLAY=:0 \
    --env UDX11_PASSWD=dockerpass \
    --volume udx11_storage:/home/udx11/storage \
    --publish 6080:6080 \
    -it yarflam/udx11
```

### 2. Gitlab Repository

1) Clone the project:

```bash
git clone https://gitlab.com/Yarflam/ubuntu-dockerx11
```

2) Execute the bash script:

```bash
chmod +x main.sh &&\
./main.sh start
```

The bash main.sh script consists of a few commands to control the application. You can type `./main.sh help` for more information.

## Usage

Open your website:

`https://my_hostname:6080/vnc.html?host=my_hostname&port=6080`

The default password is: `dockerpass`.

![udx11 capture](capture.jpg)

<div align="center">
Screen 1920x1080
</div>

## Platforms

- [Ubuntu Focal (x86-x64), Fluxbox](https://gitlab.com/Yarflam/ubuntu-dockerx11/-/tree/master)
- [Raspbian Buster (ARMv7), LXDE PIXEL](https://gitlab.com/Yarflam/ubuntu-dockerx11/-/tree/raspbian-buster)

## Authors

-   Yarflam - _initial work_

## License

The project has no license.
